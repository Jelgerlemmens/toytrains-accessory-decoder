#include <NmraDcc.h>
#include "UserConfig.h"

#define VERSION_ID                     1
#define OPS_MODE_ADDRESS_BASE_CV       0
#define EXT_INTERRUPT_NUM              0
#define ENABLE_PULLUP                  0

NmraDcc Dcc;

/*https://dccwiki.com/Accessory_Address
 * 
 *   You Controlling a Accessory Decoder** with **< a ADDRESS SUBADDRESS ACTIVATE >
    <: Begin DCC++ command
    
    a (lower case a) this command is for a Acessory Decoder
    
    ADDRESS: the primary address of the decoder controlling this turnout (0-511)
    
    SUBADDRESS: the subaddress of the decoder controlling this turnout (0-3)
    
    ACTIVATE: (0) (Deactivate, Off, Unthrown) or (1) (Activate, On, Thrown)

    >: End DCC++ command
 * 
 * 
  */

uint16_t BoardAddress = 1;
uint8_t SWITCH_0_STATE = 0;
uint8_t SWITCH_1_STATE = 0;
uint8_t SWITCH_2_STATE = 0;


void setup(){
  #ifdef DEBUG
    SERIALINTERFACE.begin(9600);
    SERIALINTERFACE.println("Starting init....");
  #endif

  pinMode(DCC_INPUT_PIN, INPUT);
  pinMode(OUTPUTPAIR_0_PIN_A, OUTPUT);
  pinMode(OUTPUTPAIR_0_PIN_B, OUTPUT);
  pinMode(OUTPUTPAIR_1_PIN_A, OUTPUT);
  pinMode(OUTPUTPAIR_1_PIN_B, OUTPUT);
  pinMode(OUTPUTPAIR_2_PIN_A, OUTPUT);
  pinMode(OUTPUTPAIR_2_PIN_B, OUTPUT);

    
  digitalWrite(OUTPUTPAIR_0_PIN_A, LOW);
  digitalWrite(OUTPUTPAIR_0_PIN_B, LOW);
  digitalWrite(OUTPUTPAIR_1_PIN_A, LOW);
  digitalWrite(OUTPUTPAIR_1_PIN_B, LOW);
  digitalWrite(OUTPUTPAIR_2_PIN_A, LOW);
  digitalWrite(OUTPUTPAIR_2_PIN_B, LOW);

  Dcc.pin(EXT_INTERRUPT_NUM, digitalPinToInterrupt(DCC_INPUT_PIN), ENABLE_PULLUP);
  Dcc.init(MAN_ID_DIY, VERSION_ID, FLAGS_DCC_ACCESSORY_DECODER, OPS_MODE_ADDRESS_BASE_CV);

  #ifdef DEBUG
    SERIALINTERFACE.println("STARTED!");
  #endif
}

void loop() {
     Dcc.process();   
}



void notifyDccAccTurnoutBoard( uint16_t BoardAddr, uint8_t OutputPair, uint8_t Direction, uint8_t OutputPower ){
  #ifdef DEBUG
    SERIALINTERFACE.print("RECEIVED: Addr:");
    SERIALINTERFACE.print(BoardAddr,DEC);
    SERIALINTERFACE.print(" OutputPair: ");
    SERIALINTERFACE.print(OutputPair, DEC);
    SERIALINTERFACE.print(" Direction: ");
    SERIALINTERFACE.print(Direction, DEC);
    SERIALINTERFACE.print(" OutputPower: ");
    SERIALINTERFACE.println(OutputPower, DEC);
  #endif
  
  if(BoardAddress == BoardAddr){
    switchOutputPair(OutputPair);
  }   
}

void switchOutputPair(uint8_t OutputPair){
  switch(OutputPair){
    case 0 :
      if(SWITCH_0_STATE == 1){
        digitalWrite(OUTPUTPAIR_0_PIN_A, HIGH);
        digitalWrite(OUTPUTPAIR_0_PIN_B, LOW);
        delay(100);
        digitalWrite(OUTPUTPAIR_0_PIN_A, LOW);
        digitalWrite(OUTPUTPAIR_0_PIN_B, LOW);
        SWITCH_0_STATE = 0;
      }else{
        digitalWrite(OUTPUTPAIR_0_PIN_A, LOW);
        digitalWrite(OUTPUTPAIR_0_PIN_B, HIGH);
        delay(100);
        digitalWrite(OUTPUTPAIR_0_PIN_A, LOW);
        digitalWrite(OUTPUTPAIR_0_PIN_B, LOW);
        SWITCH_0_STATE = 1;
      }
      #ifdef DEBUG        
        SERIALINTERFACE.print("Switched: ");
        SERIALINTERFACE.print(OutputPair, DEC);
        SERIALINTERFACE.print(" to: ");
        SERIALINTERFACE.println(SWITCH_0_STATE, DEC);
      #endif
      break;
    case 1 :
       if(SWITCH_1_STATE == 1){
        digitalWrite(OUTPUTPAIR_1_PIN_A, HIGH);
        digitalWrite(OUTPUTPAIR_1_PIN_B, LOW);
        delay(100);
        digitalWrite(OUTPUTPAIR_1_PIN_A, LOW);
        digitalWrite(OUTPUTPAIR_1_PIN_B, LOW);
        SWITCH_1_STATE = 0;
      }else{
        digitalWrite(OUTPUTPAIR_1_PIN_A, LOW);
        digitalWrite(OUTPUTPAIR_1_PIN_B, HIGH);
        delay(100);
        digitalWrite(OUTPUTPAIR_1_PIN_A, LOW);
        digitalWrite(OUTPUTPAIR_1_PIN_B, LOW);
        SWITCH_1_STATE = 1;
      }
      #ifdef DEBUG        
        SERIALINTERFACE.print("Switched: ");
        SERIALINTERFACE.print(OutputPair, DEC);
        SERIALINTERFACE.print(" to: ");
        SERIALINTERFACE.println(SWITCH_1_STATE, DEC);
      #endif   
      break;
    case 2 :
       if(SWITCH_2_STATE == 1){
        digitalWrite(OUTPUTPAIR_2_PIN_A, HIGH);
        digitalWrite(OUTPUTPAIR_2_PIN_B, LOW);
        delay(100);
        digitalWrite(OUTPUTPAIR_2_PIN_A, LOW);
        digitalWrite(OUTPUTPAIR_2_PIN_B, LOW);
        SWITCH_2_STATE = 0;
      }else{
        digitalWrite(OUTPUTPAIR_2_PIN_A, LOW);
        digitalWrite(OUTPUTPAIR_2_PIN_B, HIGH);
        delay(100);
        digitalWrite(OUTPUTPAIR_2_PIN_A, LOW);
        digitalWrite(OUTPUTPAIR_2_PIN_B, LOW);
        SWITCH_2_STATE = 1;
      }
      #ifdef DEBUG        
        SERIALINTERFACE.print("Switched: ");
        SERIALINTERFACE.print(OutputPair, DEC);
        SERIALINTERFACE.print(" to: ");
        SERIALINTERFACE.println(SWITCH_1_STATE, DEC);
      #endif            
      break;
    case 3 : 
      break;
    default:
      break;
     return;
  }  
  
}
