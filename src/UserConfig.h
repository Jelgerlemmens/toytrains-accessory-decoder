//--------------------config--------------------
#include <Arduino.h>
#ifndef USERCONFH
#define USERCONFH

        //NANO
        #if defined ARDUINO_AVR_NANO          
            #define DCC_INPUT_PIN                   2
            #define OUTPUTPAIR_0_PIN_A              6
            #define OUTPUTPAIR_0_PIN_B              7
            #define OUTPUTPAIR_1_PIN_A              8
            #define OUTPUTPAIR_1_PIN_B              9
            #define OUTPUTPAIR_2_PIN_A              10
            #define OUTPUTPAIR_2_PIN_B              11
            
            
        #endif

        //debugging
     #define DEBUG
        #ifdef DEBUG
          #define SERIALINTERFACE Serial 
        #endif
#endif

//--------------------config--------------------
